package com.tcp.timetable.domain;

import com.tcp.timetable.repository.LectureTimeRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by janpr on 2016-08-22.
 */

/**
 * JPA Entity
 * LONG         idx             PRIMARY
 * VARCHAR      lecture_day
 * INT          lecture_period
 * LONG         lecture_idx     FOREIGN
 *
 * CHECK: equal method does not calculate invisible column(join column; lecture_idx)
 */
@Entity
public class LectureTime {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String lectureDay;

    @Getter @Setter
    private int lecturePeriod;


    public LectureTime() {}

    public LectureTime(String lectureDay, int lecturePeriod) {
        this.lectureDay = lectureDay;
        this.lecturePeriod = lecturePeriod;
    }

    public static Set<LectureTime> lectureTimeParser(String origPeriod) {
        Set<LectureTime> lectureTimeList = new HashSet();

        String daySplitReg = "(월|화|수|목|금)\\(([0-9]| |~|,)+\\)";

        Pattern pattern = Pattern.compile(daySplitReg);
        Matcher matcher = pattern.matcher(origPeriod);

        List<String> dayTimeList = new ArrayList();

        while (matcher.find()) {
            dayTimeList.add(matcher.group());
        }

        for (String dayTime : dayTimeList) {
            String day, periods;
            day = dayTime.substring(0, 1);
            periods = dayTime.substring(2, dayTime.length() - 1);


            String [] inDayPeriods;
            inDayPeriods = periods.split(", ");


            List<String> periodResults = new ArrayList();
            for (String p : inDayPeriods) {
                List<String> continuousPeriods = Arrays.asList(p.split(" ~ "));

                if(continuousPeriods.size() == 2) {
                    int start = Integer.parseInt(continuousPeriods.get(0));
                    int end = Integer.parseInt(continuousPeriods.get(1));

                    continuousPeriods = new ArrayList();
                    for (int i = start; i <= end; i++) {
                        continuousPeriods.add(String.valueOf(i));
                    }
                }
                periodResults.addAll(continuousPeriods);
            }

            for (String p : periodResults) {
                LectureTime lectureTime = new LectureTime();
                lectureTime.setLectureDay(day);
                lectureTime.setLecturePeriod(Integer.parseInt(p));

                lectureTimeList.add(lectureTime);
            }
        }

        return lectureTimeList;
    }

    @Override
    public String toString() {
        return "LectureTime{" +
                "idx=" + idx +
                ", lectureDay='" + lectureDay + '\'' +
                ", lecturePeriod=" + lecturePeriod +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LectureTime)) return false;

        LectureTime that = (LectureTime) o;

        if (idx != that.idx) return false;
        if (lecturePeriod != that.lecturePeriod) return false;
        return lectureDay != null ? lectureDay.equals(that.lectureDay) : that.lectureDay == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (lectureDay != null ? lectureDay.hashCode() : 0);
        result = 31 * result + lecturePeriod;
        return result;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (!(o instanceof LectureTime)) return false;
//
//        LectureTime lt = (LectureTime)o;
//
//        if (!(this.lectureDay.equals(lt.lectureDay))) return false;
//        if (this.lecturePeriod != lt.lecturePeriod) return false;
//
//        return true;
//    }
}
