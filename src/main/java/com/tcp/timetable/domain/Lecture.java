package com.tcp.timetable.domain;

import com.fasterxml.jackson.databind.util.JSONPObject;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;

/**
 * Created by janpr on 2016-08-22.
 */

@Entity
public class Lecture {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String lectureCode;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "major_index")
    @Getter @Setter
    private Major major;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "subject_index")
    @Getter @Setter
    private Subject subject;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "professor_index")
    @Getter @Setter
    private Set<Professor> professors;

    @Getter @Setter
    private int limittedNumber;

    @Getter @Setter
    private String lectureRoom;

    @Getter @Setter
    private String clas;

    @Getter @Setter
    private boolean isMajorOnly;

    @Getter @Setter
    private String createSemester;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "lecture_time_index")
    @Getter @Setter
    private Set<LectureTime> lectureTimes;

    // TODO: lecture time을 enum type 2가지(요일, 교시)로 분리


    public Lecture() {}

    public Lecture(String lectureCode, Major major, Subject subject, Set<Professor> professors,
                   int limittedNumber, String lectureRoom, String clas, boolean isMajorOnly,
                   String createSemester, Set<LectureTime> lectureTimes) {
        this.lectureCode = lectureCode;
        this.major = major;
        this.subject = subject;
        this.professors = professors;
        this.limittedNumber = limittedNumber;
        this.lectureRoom = lectureRoom;
        this.clas = clas;
        this.isMajorOnly = isMajorOnly;
        this.createSemester = createSemester;
        this.lectureTimes = lectureTimes;
    }

    public void addProfessors(Professor professor) {
        if (this.professors == null) {
            this.professors = new HashSet();
        }
        this.professors.add(professor);
    }

    public void addLectureTimes(LectureTime lectureTime) {
        if (this.lectureTimes == null) {
            this.lectureTimes = new HashSet();
        }
        this.lectureTimes.add(lectureTime);
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "idx=" + idx +
                ", lectureCode='" + lectureCode + '\'' +
                ", major=" + major +
                ", subject=" + subject +
                ", professors=" + professors +
                ", limittedNumber=" + limittedNumber +
                ", lectureRoom='" + lectureRoom + '\'' +
                ", clas='" + clas + '\'' +
                ", isMajorOnly=" + isMajorOnly +
                ", createSemester='" + createSemester + '\'' +
                ", lectureTimes=" + lectureTimes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lecture)) return false;

        Lecture lecture = (Lecture) o;

        if (idx != lecture.idx) return false;
        if (limittedNumber != lecture.limittedNumber) return false;
        if (isMajorOnly != lecture.isMajorOnly) return false;
        if (lectureCode != null ? !lectureCode.equals(lecture.lectureCode) : lecture.lectureCode != null) return false;
        if (major != null ? !major.equals(lecture.major) : lecture.major != null) return false;
        if (subject != null ? !subject.equals(lecture.subject) : lecture.subject != null) return false;
        if (professors != null ? !professors.equals(lecture.professors) : lecture.professors != null) return false;
        if (lectureRoom != null ? !lectureRoom.equals(lecture.lectureRoom) : lecture.lectureRoom != null) return false;
        if (clas != null ? !clas.equals(lecture.clas) : lecture.clas != null) return false;
        if (createSemester != null ? !createSemester.equals(lecture.createSemester) : lecture.createSemester != null)
            return false;
        return lectureTimes != null ? lectureTimes.equals(lecture.lectureTimes) : lecture.lectureTimes == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (lectureCode != null ? lectureCode.hashCode() : 0);
        result = 31 * result + (major != null ? major.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (professors != null ? professors.hashCode() : 0);
        result = 31 * result + limittedNumber;
        result = 31 * result + (lectureRoom != null ? lectureRoom.hashCode() : 0);
        result = 31 * result + (clas != null ? clas.hashCode() : 0);
        result = 31 * result + (isMajorOnly ? 1 : 0);
        result = 31 * result + (createSemester != null ? createSemester.hashCode() : 0);
        result = 31 * result + (lectureTimes != null ? lectureTimes.hashCode() : 0);
        return result;
    }
//    @Override
//    public boolean equals(Object o) {
//        if (!(o instanceof Lecture)) return false;
//
//        Lecture l = (Lecture)o;
//
//        List<Professor> p1 = new ArrayList(professors);
//        List<Professor> p2 = new ArrayList(l.professors);
//
//        if (this.lectureCode != l.lectureCode) return false;
//        if (!this.major.equals(l.major)) return false;
//        if (!this.subject.equals(l.subject)) return false;
////        if (!(p1.containsAll(p2) && p2.containsAll(p1))) return false;
//        if (!(this.professors.containsAll(l.professors) && l.professors.containsAll(this.professors))) return false;
//        if (this.limittedNumber != l.limittedNumber) return false;
//        if (!this.lectureRoom.equals(l.lectureCode)) return false;
//        if (!this.clas.equals(l.clas)) return false;
//        if (this.isMajorOnly != l.isMajorOnly) return false;
//        if (!this.createSemester.equals(l.createSemester)) return false;
//        if (!(this.lectureTimes.containsAll(l.lectureTimes) && l.lectureTimes.containsAll(this.lectureTimes))) return false;
//
//        return true;
//    }
}
