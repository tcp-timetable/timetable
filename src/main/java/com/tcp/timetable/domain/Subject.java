package com.tcp.timetable.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by janpr on 2016-08-22.
 */

@Entity
public class Subject {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String subjectCode;

    @Getter @Setter
    private String subjectName;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Getter @Setter
    private SubjectType subjectType;

    @Getter @Setter
    private int unit;


    public Subject() {}

    public Subject(String subjectCode, String subjectName, SubjectType subjectType, int unit) {
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
        this.subjectType = subjectType;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "idx=" + idx +
                ", subjectCode='" + subjectCode + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", subjectType=" + subjectType +
                ", unit=" + unit +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subject)) return false;

        Subject subject = (Subject) o;

        if (idx != subject.idx) return false;
        if (unit != subject.unit) return false;
        if (subjectCode != null ? !subjectCode.equals(subject.subjectCode) : subject.subjectCode != null) return false;
        if (subjectName != null ? !subjectName.equals(subject.subjectName) : subject.subjectName != null) return false;
        return subjectType != null ? subjectType.equals(subject.subjectType) : subject.subjectType == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (subjectCode != null ? subjectCode.hashCode() : 0);
        result = 31 * result + (subjectName != null ? subjectName.hashCode() : 0);
        result = 31 * result + (subjectType != null ? subjectType.hashCode() : 0);
        result = 31 * result + unit;
        return result;
    }
}
