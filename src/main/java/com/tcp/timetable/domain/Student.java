package com.tcp.timetable.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by janpr on 2016-08-22.
 */

/**
 * 학번, 암호, 전공, 복학상태 등 저장
 */

@Entity
public class Student {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String studentNum;

    @Getter @Setter
    private String password;

    @ManyToOne
    @Getter @Setter
    private Major major;

    @Getter @Setter
    private int status;

    @ManyToMany
    @Getter @Setter
    private List<Timetable> timetables;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (idx != student.idx) return false;
        if (status != student.status) return false;
        if (studentNum != null ? !studentNum.equals(student.studentNum) : student.studentNum != null) return false;
        if (password != null ? !password.equals(student.password) : student.password != null) return false;
        if (major != null ? !major.equals(student.major) : student.major != null) return false;
        return timetables != null ? timetables.equals(student.timetables) : student.timetables == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (studentNum != null ? studentNum.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (major != null ? major.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (timetables != null ? timetables.hashCode() : 0);
        return result;
    }
}
