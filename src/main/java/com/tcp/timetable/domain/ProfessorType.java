package com.tcp.timetable.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by janpr on 2016-08-22.
 */

@Entity
public class ProfessorType {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String type;


    public ProfessorType() {}

    public ProfessorType(String type) {
        this.type = type;

    }

    @Override
    public String toString() {
        return "ProfessorType{" +
                "idx=" + idx +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfessorType)) return false;

        ProfessorType that = (ProfessorType) o;

        if (idx != that.idx) return false;
        return type != null ? type.equals(that.type) : that.type == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
