package com.tcp.timetable.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.sql.Date;
import java.util.List;

/**
 * Created by janpr on 2016-08-22.
 */

@Entity
public class Timetable {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @ManyToMany
    @Getter @Setter
    private List<Lecture> lectures;

    @Getter @Setter
    private Date createDate;

    @Getter @Setter
    private String createSemester;
}
