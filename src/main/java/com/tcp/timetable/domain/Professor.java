package com.tcp.timetable.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by janpr on 2016-08-22.
 */

@Entity
public class Professor {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String professorName;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "professor_type_index")
    @Getter @Setter
    private ProfessorType professorType;

    @Getter @Setter
    private long stechProfessorIndex;


    public Professor() {}

    public Professor(String professorName, ProfessorType professorType) {
        this.professorName = professorName;
        this.professorType = professorType;
    }

    @Override
    public String toString() {
        return "Professor{" +
                "idx=" + idx +
                ", professorName='" + professorName + '\'' +
                ", professorType=" + professorType +
                ", stechProfessorIndex=" + stechProfessorIndex +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Professor)) return false;

        Professor professor = (Professor) o;

        if (idx != professor.idx) return false;
        if (stechProfessorIndex != professor.stechProfessorIndex) return false;
        if (professorName != null ? !professorName.equals(professor.professorName) : professor.professorName != null)
            return false;
        return professorType != null ? professorType.equals(professor.professorType) : professor.professorType == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (professorName != null ? professorName.hashCode() : 0);
        result = 31 * result + (professorType != null ? professorType.hashCode() : 0);
        result = 31 * result + (int) (stechProfessorIndex ^ (stechProfessorIndex >>> 32));
        return result;
    }
}
