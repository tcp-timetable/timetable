package com.tcp.timetable.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by janpr on 2016-08-22.
 */

@Entity
public class Major {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String majorCode;

    @Getter @Setter
    private String majorName;


    public Major() {}

    public Major(String majorCode, String majorName) {
        this.majorCode = majorCode;
        this.majorName = majorName;
    }

    @Override
    public String toString() {
        return "Major{" +
                "idx=" + idx +
                ", majorCode='" + majorCode + '\'' +
                ", majorName='" + majorName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Major)) return false;

        Major major = (Major) o;

        if (idx != major.idx) return false;
        if (majorCode != null ? !majorCode.equals(major.majorCode) : major.majorCode != null) return false;
        return majorName != null ? majorName.equals(major.majorName) : major.majorName == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (majorCode != null ? majorCode.hashCode() : 0);
        result = 31 * result + (majorName != null ? majorName.hashCode() : 0);
        return result;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (!(o instanceof Major)) return false;
//
//        Major m = (Major)o;
//
//        if (!(this.majorCode.equals(m.majorCode))) return false;
//        if (!(this.majorName.equals(m.majorName))) return false;
//
//        return true;
//    }
}
