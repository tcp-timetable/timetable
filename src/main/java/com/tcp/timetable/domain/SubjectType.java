package com.tcp.timetable.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by janpr on 2016-08-22.
 */

@Entity
public class SubjectType {
    @Id
    @GeneratedValue
    @Getter @Setter
    private long idx;

    @Getter @Setter
    private String subjectTypeCode;

    @Getter @Setter
    private String type;

    @Getter @Setter
    private boolean isMajorNecessary;


    public SubjectType() {}

    public SubjectType(String subjectTypeCode, String type, boolean isMajorNecessary) {
        this.subjectTypeCode = subjectTypeCode;
        this.type = type;
        this.isMajorNecessary = isMajorNecessary;
    }

    @Override
    public String toString() {
        return "SubjectType{" +
                "idx=" + idx +
                ", subjectTypeCode='" + subjectTypeCode + '\'' +
                ", type='" + type + '\'' +
                ", isMajorNecessary=" + isMajorNecessary +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubjectType)) return false;

        SubjectType that = (SubjectType) o;

        if (idx != that.idx) return false;
        if (isMajorNecessary != that.isMajorNecessary) return false;
        if (subjectTypeCode != null ? !subjectTypeCode.equals(that.subjectTypeCode) : that.subjectTypeCode != null)
            return false;
        return type != null ? type.equals(that.type) : that.type == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idx ^ (idx >>> 32));
        result = 31 * result + (subjectTypeCode != null ? subjectTypeCode.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (isMajorNecessary ? 1 : 0);
        return result;
    }
}
