package com.tcp.timetable.repository;

import com.tcp.timetable.domain.SubjectType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by tcp on 2016-07-13.
 */

public interface SubjectTypeRepository extends JpaRepository<SubjectType, Long> {

//    @Query(value = "SELECT * FROM SUBJECT_TYPE st WHERE st.SUBJECT_TYPE_CODE = ?1", nativeQuery = true)
    SubjectType findBySubjectTypeCode(String subjectTypeCode);
}
