package com.tcp.timetable.repository;

import com.tcp.timetable.domain.Major;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by tcp on 2016-07-13.
 */

public interface MajorRepository extends JpaRepository<Major, Long> {

//    @Query(value = "select * from Major m where m.major_code = ?1", nativeQuery = true)
    Major findByMajorCode(String majorCode);
}
