package com.tcp.timetable.repository;

import com.tcp.timetable.domain.ProfessorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by tcp on 2016-07-13.
 */

public interface ProfessorTypeRepository extends JpaRepository<ProfessorType, Long> {

//    @Query(value = "select * from Professor_type pt where pt.type = ?1", nativeQuery = true)
    ProfessorType findByType(String type);
}
