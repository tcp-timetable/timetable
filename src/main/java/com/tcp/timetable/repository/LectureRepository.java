package com.tcp.timetable.repository;

import com.tcp.timetable.domain.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by tcp on 2016-07-13.
 */

public interface LectureRepository extends JpaRepository<Lecture, Long> {

//    @Query(value = "select * from lecture l where l.lecture_code = ?1 and l.subject_index = ?2 and l.major_index = ?3", nativeQuery = true)
//    Lecture findWith(String lectureCode, int subjectIndex, int majorIndex);

    Lecture findByLectureCodeAndSubjectIdxAndMajorIdx(String lectureCode, Long subjectIndex, Long majorIndex);

//    List<Lecture> findAll();
}
