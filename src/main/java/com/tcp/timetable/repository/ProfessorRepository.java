package com.tcp.timetable.repository;

import com.tcp.timetable.domain.Professor;
import com.tcp.timetable.domain.ProfessorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by tcp on 2016-07-13.
 */

public interface ProfessorRepository extends JpaRepository<Professor, Long> {

//    @Query(value = "select * from Professor p where p.professor_name = ?1 and p.professor_type_index = ?2", nativeQuery = true)
    Professor findByProfessorNameAndProfessorTypeIdx(String professorName, long professorTypeIndex);
}
