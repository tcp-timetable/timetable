package com.tcp.timetable.repository;

import com.tcp.timetable.domain.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by JHD on 2016-05-21.
 */

public interface SubjectRepository extends JpaRepository<Subject, Long> {

//    @Query(value = "select * from Subject s where s.subject_code = ?1", nativeQuery = true)
    Subject findBySubjectCode(String subjectCode);

//    @Query(value = "select * from Subject s where s.subject_code = ?1 and s.major_index = ?2", nativeQuery = true)
//    Subject findByCodeAndMajorIndex(String subjectCode, int majorIndex);
}
