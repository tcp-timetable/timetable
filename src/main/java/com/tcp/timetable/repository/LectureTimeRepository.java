package com.tcp.timetable.repository;

import com.tcp.timetable.domain.LectureTime;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tcp on 2016-07-13.
 */

public interface LectureTimeRepository extends JpaRepository<LectureTime, Long> {

    LectureTime findByLectureDayAndLecturePeriod(String lectureDay, int lecturePeriod);
}
