package com.tcp.timetable.repository;

import com.tcp.timetable.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by JHD on 2016-05-21.
 */

public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByStudentNum(String studentNum);
}
