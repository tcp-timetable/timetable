package com.tcp.timetable.xml_parser.lecture;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by tcp on 2016-06-28.
 */
public class Row {
    @Getter
    @Setter
    private Col[] col;
}
