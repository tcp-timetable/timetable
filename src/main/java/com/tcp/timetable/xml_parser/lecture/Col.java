package com.tcp.timetable.xml_parser.lecture;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by tcp on 2016-06-28.
 */
public class Col {
    @Getter
    @Setter
    private String id;
    @Getter
    @Setter
    private String value;
}
