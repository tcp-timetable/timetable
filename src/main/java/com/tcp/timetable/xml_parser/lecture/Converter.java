package com.tcp.timetable.xml_parser.lecture;

import org.springframework.core.io.*;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.castor.CastorMarshaller;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

/**
 * Created by tcp on 2016-06-27.
 */
public class Converter <T> {
    public static String DEFAULT_FILE_LOCATION = "C:/temp/data.xml";
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;
    private CastorMarshaller castorMarshaller;
    private final Class<T> genericType;

//    public Converter(String mappingLocation, String mappingPackage) {
//        this.genericType = (Class<T>)((ParameterizedType)getClass().getGenericSuperClass()).getActualTypeArguments()[0];
//        System.out.println(this.genericType);
//
//        castorMarshaller = new CastorMarshaller();
//        castorMarshaller.setTargetClass(this.genericType);
//        castorMarshaller.setMappingLocation(new ClassPathResource(mappingLocation));
//        castorMarshaller.setTargetPackage(mappingPackage);
//        try {
//            castorMarshaller.afterPropertiesSet();
//        } catch (IOException e) {
//            System.out.println(e);
//        }
//
//        this.setMarshaller(castorMarshaller);
//        this.setUnmarshaller(castorMarshaller);
//    }

    public Converter(Class<T> genericType, String mappingLocation, String mappingPackage) {
        this.genericType = genericType;

        castorMarshaller = new CastorMarshaller();
        castorMarshaller.setTargetClass(this.genericType);
//        castorMarshaller.setMappingLocation(new ClassPathResource(mappingLocation));
        castorMarshaller.setMappingLocation(new PathResource(System.getProperty("user.dir").toString() + "/src/main/" + mappingLocation));
        castorMarshaller.setTargetPackage(mappingPackage);
        try {
            castorMarshaller.afterPropertiesSet();
        } catch (IOException e) {
            System.out.println(e);
        }

        this.setMarshaller(castorMarshaller);
        this.setUnmarshaller(castorMarshaller);
    }

    public void setMarshaller(Marshaller marshaller) {
        this.marshaller = marshaller;
    }

    public void setUnmarshaller(Unmarshaller unmarshaller) {
        this.unmarshaller = unmarshaller;
    }

    public void save(String fileLocation, T data) {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(fileLocation);
//            System.out.println("construct fos");

            this.marshaller.marshal(data, new StreamResult(fos));
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public T load(String fileLocation) {
        FileInputStream fis;
        T data = (T)new Object();
        try {
            fis = new FileInputStream(fileLocation);
//            System.out.println("construct fis");

            data = (T)this.unmarshaller.unmarshal(new StreamSource(fis));

            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }

        return data;
    }
}
