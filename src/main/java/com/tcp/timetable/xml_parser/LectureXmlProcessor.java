package com.tcp.timetable.xml_parser;


import com.tcp.timetable.xml_parser.lecture.Col;
import com.tcp.timetable.xml_parser.lecture.Converter;
import com.tcp.timetable.xml_parser.lecture.Row;
import com.tcp.timetable.xml_parser.lecture.Rows;
import com.tcp.timetable.domain.*;
import com.tcp.timetable.repository.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by tcp on 2016-07-13.
 *
 * parse seoultech lecture XML data from xml to JPA Entity
 */
@Service
public class LectureXmlProcessor {
    @Autowired
    LectureTimeRepository lectureTimeRepository;


    /**
     * parse lecture xml to Lecture Set (JPA Entity) include other related Entity
     * * instantiated Entities in Set are not persisted
     *
     * used library Spring CastorMarshaller for xml parsing
     *
     * @param xmlFileName
     * @return lectureSet
     */
    public Set<Lecture> parseLectureXml(String xmlFileName) {
        Converter<Rows> converter = new Converter(Rows.class, "lesson_mapping.xml", "com.tcp.timetable.Stech.XmlMarshaller");

        Rows loadData;
        loadData = converter.load(System.getProperty("user.dir").toString() + "/src/main/" + xmlFileName);

        Set<Lecture> allLectureSet = new HashSet();
        Set<Subject> allSubjectSet = new HashSet();
        Set<SubjectType> allSubjectTypeSet = new HashSet();
        Set<Major> allMajorSet = new HashSet();
        Set<Professor> allProfessorSet = new HashSet();
        Set<ProfessorType> allProfessorTypeSet = new HashSet();

        Row[] rows = loadData.getRow();
        for (Row r : rows) {
            Col[] cols = r.getCol();

            Map<String, String> lectureData = new HashMap();

            for (Col c : cols) {
                String id, value;
                id = c.getId();
                value = c.getValue();

                lectureData.put(id, value);
            }

            String rawSubjectTypeCode = lectureData.get("CMPT_DIV");
            String rawSubjectTypeName = lectureData.get("CMPT_DIV_NM");
            String rawSubjectCode = lectureData.get("SUBJ_CD");
            String rawSubjectName = lectureData.get("SUBJ_CD_NM");
            String rawCreateSemester = lectureData.get("SP_YMST");
            String rawLectureCode = lectureData.get("LECT_NUMB");
            int rawLectureUnit = Integer.parseInt(lectureData.get("UNIT"));
            String rawLimittedNumber = lectureData.get("COUR_INWON");
            String rawMajorCode = lectureData.get("LESS_CD");
            String rawMajorName = lectureData.get("ORGN_NM");
            String rawProfessorTypeType = lectureData.get("PST_LIST");
            String rawProfessorName = lectureData.get("PROF_LIST");
            String rawLectureRoom = lectureData.get("BLDG_COUM_NM");
            String rawLectureTime = lectureData.get("LSTM_LIST");

            if (rawProfessorTypeType == null) rawProfessorTypeType = "";
            if (rawProfessorName == null) rawProfessorName = "";
            if (rawLectureTime == null) rawLectureTime = "";

            int limittedNum = Integer.parseInt(rawLimittedNumber.substring(0, rawLimittedNumber.indexOf('(')));

            // all domain objects are stored in Set (for persistence of equivalent object)
            SubjectType subjectType;
            List<SubjectType> allSubjectTypeList = new ArrayList(allSubjectTypeSet);
            int stIdx = allSubjectTypeList.indexOf(new SubjectType(rawSubjectTypeCode, rawSubjectTypeName, false));
            if (stIdx < 0) {
                subjectType = new SubjectType(rawSubjectTypeCode, rawSubjectTypeName, false);
                allSubjectTypeSet.add(subjectType);
            } else {
                subjectType = allSubjectTypeList.get(stIdx);
            }

            Subject subject;
            List<Subject> allSubjectList = new ArrayList(allSubjectSet);
            int sIdx = allSubjectList.indexOf(new Subject(rawSubjectCode, rawSubjectName, subjectType, rawLectureUnit));
            if (sIdx < 0) {
                subject = new Subject(rawSubjectCode, rawSubjectName, subjectType, rawLectureUnit);
                allSubjectSet.add(subject);
            } else {
                subject = allSubjectList.get(sIdx);
            }

            Major major;
            List<Major> allMajorList = new ArrayList(allMajorSet);
            int mIdx = allMajorList.indexOf(new Major(rawMajorCode, rawMajorName));
            if (mIdx < 0) {
                major = new Major(rawMajorCode, rawMajorName);
                allMajorSet.add(major);
            } else {
                major = allMajorList.get(mIdx);
            }

            List<ProfessorType> professorTypes = new ArrayList();
            List<ProfessorType> allProfessorTypeList = new ArrayList(allProfessorTypeSet);
            List<String> professorTypeTypes = Arrays.asList(rawProfessorTypeType.split(", "));
            for (String pt : professorTypeTypes) {
                int ptIdx = allProfessorTypeList.indexOf(new ProfessorType(pt));
                ProfessorType professorType;
                if (ptIdx < 0) {
                    professorType = new ProfessorType(pt);
                    allProfessorTypeSet.add(professorType);
                } else {
                    professorType = allProfessorTypeList.get(ptIdx);
                }
                professorTypes.add(professorType);
            }

            Set<Professor> professors = new HashSet();
            List<Professor> allProfessorList = new ArrayList(allProfessorSet);
            List<ProfessorType> professorTypesList = new ArrayList(professorTypes);
            List<String> professorNames = Arrays.asList(rawProfessorName.split(", "));
            int i = 0;
            for (String pn : professorNames) {
                ProfessorType currentProfessorType;
                currentProfessorType = (professorTypesList.size() > i ? professorTypesList.get(i) : null);
                Professor p = new Professor(pn, currentProfessorType);
                int pIdx = allProfessorList.indexOf(p);
                Professor professor;
                if (pIdx < 0) {
                    professor = new Professor(pn, currentProfessorType);
                    allProfessorSet.add(professor);
                } else {
                    professor = allProfessorList.get(pIdx);
                }
                professors.add(professor);
                i++;
            }

            Set<LectureTime> lectureTimes = LectureTime.lectureTimeParser(rawLectureTime);

            Lecture lecture;
            List<Lecture> allLectureList = new ArrayList(allLectureSet);
            int lIdx = allLectureList.indexOf(new Lecture(rawLectureCode, major, subject, professors, limittedNum, rawLectureRoom, "", false, rawCreateSemester, lectureTimes));
            if (lIdx < 0) {
                lecture = new Lecture(rawLectureCode, major, subject, professors, limittedNum, rawLectureRoom, "", false, rawCreateSemester, lectureTimes);
                allLectureSet.add(lecture);
            } else {
//                lecture = lectureList.get(lIdx);
            }
        }

        return allLectureSet;
    }
}
