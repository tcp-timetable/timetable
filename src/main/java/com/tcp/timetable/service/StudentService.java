package com.tcp.timetable.service;

import com.tcp.timetable.domain.Student;
import com.tcp.timetable.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tcp2 on 2016-05-18.
 */

@Service
@Transactional
public class StudentService {
    @Autowired
    private StudentRepository studentRepostory;

    public boolean login(String studentNum, String password) {
        Student student = studentRepostory.findByStudentNum(studentNum);

        boolean isSuccess = false;

        if (student == null) {
            return isSuccess;
        }

        if (studentNum.equals(student.getStudentNum()) && password.equals(student.getPassword())) {
            isSuccess = true;
        }

        return isSuccess;
    }


    public void add(Student student) {
        studentRepostory.save(student);
    }
    public void add(String studentNum, String password) {
        Student student = new Student();
        student.setStudentNum(studentNum);
        student.setPassword(password);

        studentRepostory.save(student);
    }

    public Student get(long studentNum) {
        return studentRepostory.findOne(studentNum);
    }
}
