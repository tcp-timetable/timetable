package com.tcp.timetable.service;

import com.tcp.timetable.domain.Lecture;
import com.tcp.timetable.domain.LectureTime;
import com.tcp.timetable.repository.LectureRepository;
import com.tcp.timetable.xml_parser.LectureXmlProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by janpr on 2016-09-18.
 */

@Service
public class LectureService {
    @Autowired
    private LectureRepository lectureRepository;
    @Autowired
    private LectureXmlProcessor lectureXmlProcessor;

    @Autowired
    private LectureTimeService lectureTimeService;

    public void saveFromXml(String xmlPath) {
        Set<LectureTime> initData = lectureTimeService.initLectureTime();

        Set<Lecture> lectureSet = lectureXmlProcessor.parseLectureXml(xmlPath);

        lectureRepository.save(lectureSet);
    }

}
