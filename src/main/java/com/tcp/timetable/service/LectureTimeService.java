package com.tcp.timetable.service;

import com.tcp.timetable.domain.LectureTime;
import com.tcp.timetable.repository.LectureTimeRepository;
import com.tcp.timetable.xml_parser.LectureXmlProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by janpr on 2016-09-18.
 */

@Service
public class LectureTimeService {
    @Autowired
    private LectureTimeRepository lectureTimeRepository;
    @Autowired
    private LectureXmlProcessor lectureXmlProcessor;
    public final static String initLectureTimeData = "월(1 ~ 9), 화(1 ~ 9), 수(1 ~ 9), 목(1 ~ 9), 금(1 ~ 9), 토(1 ~ 9), 일(1 ~ 9)";

    public Set<LectureTime> initLectureTime() {
        Set<LectureTime> initData = LectureTime.lectureTimeParser(initLectureTimeData);

        lectureTimeRepository.save(initData);

        return initData;
    }
}
