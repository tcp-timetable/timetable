package com.tcp.timetable.controller;

import com.tcp.timetable.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tcp2 on 2016-05-18.
 */


@RestController
public class LoginController {
    @Autowired
    private StudentService studentService;

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String login(@RequestParam(name = "studentNum") String studentNum,
                        @RequestParam(name = "password") String password) {
        boolean isSuccess = studentService.login(studentNum, password);

        return (isSuccess ? "success" : "fail");
    }

    @RequestMapping(path = "/student", method = RequestMethod.POST)
    @ResponseBody
    public String add(@RequestParam(name = "studentNum") String studentNum,
                      @RequestParam(name = "password") String password) {
        studentService.add(studentNum, password);

        // test comment

        return "success";
    }

    @RequestMapping(path = "/loginSugang", method = RequestMethod.GET)
    @ResponseBody
    public String loginSugang() {
        ResponseEntity<String> res;

        return "";
    }

//    @RequestMapping(path = "/loginPortal", method = RequestMethod.GET)
//    @ResponseBody
//    public String loginPortal() {
//        SessionTemplate stechRestTemplate = new SessionTemplate();
//
//        HttpEntity<String> response = stechRestTemplate.loginPortal();
//
//        return response.getHeaders().toString();
//    }
//
//    @RequestMapping(path = "/loginSUIS", method = RequestMethod.GET)
//    @ResponseBody
//    public String loginSUIS() {
//        SessionTemplate stechRestTemplate = new SessionTemplate();
//
//        HttpEntity<String> response = stechRestTemplate.loginPortal();
//        response = stechRestTemplate.loginSUIS(response);
//
//        return response.getBody().toString();
//    }
//
//    @RequestMapping(path = "/getStudentInfos", method = RequestMethod.GET)
//    @ResponseBody
//    public String getStudentInfos() {
//        SessionTemplate stechRestTemplate = new SessionTemplate();
//
//        HttpEntity<String> response = stechRestTemplate.loginPortal();
//        response = stechRestTemplate.loginSUIS(response);
//        response = stechRestTemplate.getStudentInfos(response);
//
//        return response.getBody().toString();
//    }
}