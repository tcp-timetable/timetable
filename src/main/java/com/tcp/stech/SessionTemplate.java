package com.tcp.stech;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpsExchange;
import org.apache.http.impl.DefaultHttpRequestFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by JHD on 2016-05-23.
 */


public class SessionTemplate {
    private RestTemplate restTemplate;
    private Map<String, String> cookieMap;

    public static final ClientHttpRequestFactory HTTP_REQUEST = new SimpleClientHttpRequestFactory();
    public static final ClientHttpRequestFactory HTTPS_REQUEST = new HttpComponentsClientHttpRequestFactory();

    public SessionTemplate() {
        // SimpleClientHttpRequestFactory
        // HttpComponentsClientHttpRequestFactory
        restTemplate = new RestTemplate();
        cookieMap = new HashMap();
    }

    public void setRequestFactory(ClientHttpRequestFactory requestFactory) {
        restTemplate.setRequestFactory(requestFactory);
    }

    public static Map<String, String> cookieHeaderMapper(String cookie) {
        Map<String, String> cookieMap = new HashMap();
        String [] cookieParams = cookie.split(";");

//        System.out.println("cookieHeaderMapper Log");
//        System.out.println(cookie);

        for (String cookieParam : cookieParams) {
//            System.out.println("split cookie params: " + cookieParam);
            String [] param = cookieParam.split("=");
            if (param.length < 2) continue;

            String key = param[0], value = param[1];

            cookieMap.put(key, value);
        }
//        System.out.println(cookieMap);
        return cookieMap;
    }

    public static String cookieHeaderMapper(Map<String, String> cookieMap) {
        String cookie = "";

//        System.out.println("cookieHeaderMapper Log");
//        System.out.println(cookieMap);
//
        for (String key : cookieMap.keySet()) {
            cookie += (key + "=" + cookieMap.get(key) + ";");
        }
//        System.out.println(cookie);
        return cookie;
    }

    private ResponseEntity<String> exchange(String url, HttpMethod method, HttpHeaders additionalHeaders, JSONObject json, Map<String, String> urlParams) {
        if (additionalHeaders == null) additionalHeaders = new HttpHeaders();
        if (additionalHeaders.get("Cookie") == null) additionalHeaders.set("Cookie", "");

        List<String> additionalCookies = additionalHeaders.get("Cookie");
        if (additionalCookies != null && additionalCookies.size() == 1) {
            Map<String, String> additionalCookieMap = cookieHeaderMapper(additionalCookies.get(0));
            for (String key : additionalCookieMap.keySet()) {
                this.cookieMap.put(key, additionalCookieMap.get(key));
            }
        }

        HttpHeaders requestHeaders = new HttpHeaders();
        for (String key : additionalHeaders.keySet()) {
            List<String> values = additionalHeaders.get(key);
            for (String value : values) {
                requestHeaders.add(key, value);
            }
        }
        requestHeaders.set("Cookie", this.cookieHeaderMapper(this.cookieMap));

//        System.out.println("request header log");
//        System.out.println(new JSONObject(requestHeaders).toString(4));

        HttpEntity entity = new HttpEntity(json.toString(), requestHeaders);

        if (urlParams == null) {
            urlParams = new HashMap();
        }


        ResponseEntity<String> res = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, urlParams);


        List<String> setCookies = res.getHeaders().get("Set-Cookie");
        if (setCookies != null) {
            Stream<Map<String, String>> setCookieMapStream = setCookies.stream().map(PortalSession::cookieHeaderMapper);

            setCookieMapStream.forEach(cookieMap -> {
                for (String key : cookieMap.keySet()) {
                    this.cookieMap.put(key, cookieMap.get(key));
                }
            });
        }

        return res;
    }

    public ResponseEntity<String> get(String requestUrl, HttpHeaders additionalHeaders, Map<String, String> urlParams) {
        ResponseEntity<String> res;

        res = this.exchange(requestUrl, HttpMethod.GET, additionalHeaders, new JSONObject(), urlParams);

        return res;
    }

    public ResponseEntity<String> post(String requestUrl, HttpHeaders additionalHeaders, JSONObject json, Map<String, String> urlParams) {
        ResponseEntity<String> res;

        res = this.exchange(requestUrl, HttpMethod.POST, additionalHeaders, json, urlParams);

        return res;
    }

    public Map<String, String> getCookieMap() {
        Map<String, String> map = new HashMap(this.cookieMap);

        return map;
    }
}