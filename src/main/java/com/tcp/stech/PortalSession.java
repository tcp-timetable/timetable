package com.tcp.stech;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.security.Principal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Created by tcp on 2016-09-25.
 */
public class PortalSession extends SessionTemplate {

    public static String PORTAL_URL = "http://portal.seoultech.ac.kr";

    public HttpEntity<String> loginPortal(String id, String password) throws IOException {
        FileOutputStream fos = new FileOutputStream("loginPortalBody.log");
        String text = new String();

        HttpEntity res;
        String url;

        // Login Page GET Request and Cookie Setting
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//        headers.set("Accept-Encoding", "gzip, deflate, sdch");
        headers.set("Accept-Language", "ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4");
        headers.set("Cache-Control", "no-cache");
        headers.set("Connection", "keep-alive");
        headers.set("Host", "portal.seoultech.ac.kr");
//        headers.set("Pragma", "no-cache");
        headers.set("Referer", "http://portal.seoultech.ac.kr/portal/default/SEOULTECH/LOGIN");
        headers.set("Upgrade-Insecure-Requests", "1");
        headers.set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2876.1 Safari/537.36");

        url = "/";
        res = this.get(PORTAL_URL, headers, new HashMap());
        text += url + "\n";
        text += new JSONObject(res.getHeaders()).toString(4) + "\n\n";
        text += trim(res.getBody().toString()) + "\n\n";

        url = "/index.jsp";
        res = this.get(PORTAL_URL + url, headers, new HashMap());
        text += url + "\n";
        text += new JSONObject(res.getHeaders()).toString(4) + "\n\n";
        text += trim(res.getBody().toString()) + "\n\n";

//        String trimmed = trim(res.getBody().toString());
//        System.out.println("ttttt");
//        System.out.println(trimmed);
//        System.out.println("ssssssssss");
//        System.out.println(unescape(trimmed));


        String jsessionid = this.getCookieMap().get("JSESSIONID");
        url = "/portal/;jsessionid=" + jsessionid;
        res = this.get(PORTAL_URL + url, headers, new HashMap());
        text += url + "\n";
        text += new JSONObject(res.getHeaders()).toString(4) + "\n\n";
        text += trim(res.getBody().toString()) + "\n\n";

        url = "/portal/default/SEOULTECH/LOGIN";
        res = this.get(PORTAL_URL + url, headers, new HashMap());
        text += url + "\n";
        text += new JSONObject(res.getHeaders()).toString(4) + "\n\n";
        text += trim(res.getBody().toString()) + "\n\n";

        // Login POST request
        JSONObject student = new JSONObject();
        student.put("userId", id);
        student.put("password", password);
        student.put("ssoLangKnd", "ko");
        student.put("returnUrl", "");
//        student.put("lok", "로그인");
//        student.put("saveLoginID", "on");

        headers.set("Origin", "http://portal.seoultech.ac.kr");
        headers.set("Referer", "http://portal.seoultech.ac.kr/portal/default/SEOULTECH/LOGIN?returnUrl=");
        headers.set("Cookie", "evSSOCookie=loggedout;EnviewLangKnd=ko;EnviewLoginID=%3B0");
        this.setRequestFactory(this.HTTPS_REQUEST);

        url = "https://portal.seoultech.ac.kr/user/seouolTechLogin.face";
        res = this.post(url, headers, student, new HashMap());
        text += url + "\n";
        text += new JSONObject(res.getHeaders()).toString(4) + "\n\n";
        text += trim(res.getBody().toString()) + "\n\n";

        HttpHeaders resHeaders = res.getHeaders();
        this.setRequestFactory(this.HTTP_REQUEST);

        res = this.get(resHeaders.getLocation().toString(), headers, new HashMap());
        text += url + "\n";
        text += new JSONObject(res.getHeaders()).toString(4) + "\n\n";
        text += trim(res.getBody().toString()) + "\n\n";

        res = this.get(resHeaders.getLocation().toString(), headers, new HashMap());
        text += url + "\n";
        text += new JSONObject(res.getHeaders()).toString(4) + "\n\n";
        text += trim(res.getBody().toString()) + "\n\n";

        fos.write(text.getBytes("UTF-8"));
        fos.close();

        return res;
    }

    public static String trim(String str) {
        String trimmed = new String(str);
        int idx;

        String reg = "((\n|\r\n)( |\t)*(\n|\r\n))+";
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(str);

        trimmed = m.replaceAll("\r\n");

        return trimmed;



//        while ((idx = trimmed.indexOf("\n\n")) > -1) {
//            trimmed.replace("\n\n", "\n");
////            System.out.println("trimmed");
//        }
//
//        while ((idx = trimmed.indexOf("\r\n\r\n")) > -1) {
//            trimmed.replace("\r\n\r\n", "\r\n");
////            System.out.println("trimmed");
//        }
//
//        return trimmed;
    }

    public static String unescape(String input) {
        Stream<String> stream = input.chars().mapToObj(i -> Character.valueOf((char)i).toString());

        String output = "";
        output = stream.map(ch -> {
            switch (ch) {
                case "\n": return "\\n";
                case "\r": return "\\r";
                default: return ch;
            }
        }).reduce("", (accum, str) -> accum + str);

//        stream.map(ch -> {
//            switch (ch) {
//                case "\n": return "\\n";
//                case "\r": return "\\r";
//                default: return ch;
//            }
//        }).forEach(System.out::println);

        return output;
    }
}
