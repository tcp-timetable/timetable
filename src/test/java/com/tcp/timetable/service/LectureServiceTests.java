package com.tcp.timetable.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by janpr on 2016-10-07.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class LectureServiceTests {
    @Autowired
    private LectureService lectureService;

    @Test
    public void saveFromXmlTest() throws Exception {
        lectureService.saveFromXml("sugang_data.xml");
    }

}