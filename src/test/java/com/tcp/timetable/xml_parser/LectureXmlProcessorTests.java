package com.tcp.timetable.xml_parser;

import com.tcp.timetable.domain.*;
import com.tcp.timetable.repository.LectureRepository;
import com.tcp.timetable.service.LectureTimeService;
import com.tcp.timetable.xml_parser.LectureXmlProcessor;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by janpr on 2016-08-24.
 *
 * Test class for LectureXmlProcessor
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LectureXmlProcessorTests {
    @Autowired
    private LectureXmlProcessor lectureXmlProcessor;

    /**
     * test for parsing lecture time from raw string to LectureTime
     */
    @Test
    public void parseLectureTimeTest() {
        String testInput = "월(1 ~ 4), 목(4, 6 ~ 9)";

        Set<LectureTime> result = LectureTime.lectureTimeParser(testInput);

        Set<LectureTime> compareSet = new HashSet();
        compareSet.add(new LectureTime("월", 1));
        compareSet.add(new LectureTime("월", 2));
        compareSet.add(new LectureTime("월", 3));
        compareSet.add(new LectureTime("월", 4));
        compareSet.add(new LectureTime("목", 4));
        compareSet.add(new LectureTime("목", 6));
        compareSet.add(new LectureTime("목", 7));
        compareSet.add(new LectureTime("목", 8));
        compareSet.add(new LectureTime("목", 9));

        Assert.assertEquals("result has unexpected element", true, compareSet.containsAll(result));
        Assert.assertEquals("result not contain element", true, result.containsAll(compareSet));
    }


    /**
     * test for parsing lecture xml to Lecture Set
     */
    @Test
    public void parseLectureXmlDataTest() {
//        Set<Lecture> lectureSet = lectureXmlProcessor.parseLectureXml("sample_data.xml");
        Set<Lecture> lectureSet = lectureXmlProcessor.parseLectureXml("sugang_data.xml");

//        lectureSet.stream().forEach(System.out::println);
        /**
         * TODO: assert routine. 나중에 작성할래...
         */

    }
}
