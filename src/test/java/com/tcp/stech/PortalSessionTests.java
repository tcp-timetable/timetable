package com.tcp.stech;

import javassist.bytecode.ByteArray;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * Created by janpr on 2016-09-26.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.tcp.timetable.TimetableApplication.class)
public class PortalSessionTests {

    /**
     *
     * You need to create file 'user_info.txt' for portal login info at root dir
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    @Test
    public void loginTest() throws FileNotFoundException, IOException {
        PortalSession portal = new PortalSession();

        String id, password;
        char [] input = new char[16];
        InputStream fis = new FileInputStream("user_info.txt");
        InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
        BufferedReader reader = new BufferedReader(isr);

        id = reader.readLine();
        password = reader.readLine();
        System.out.println(id + " " + password);

        HttpEntity res = portal.loginPortal(id, password);

        FileOutputStream fos;
        fos = new FileOutputStream("loginTest.log");

        String text = "";
        text += "loginTest\n";
        text += res.getHeaders() + "\n\n";
        text += res.getBody() + "\n\n";

        fos.write(text.getBytes("UTF-8"));
        fos.close();

//        System.out.println("loginTest");
//        System.out.println(res.getHeaders());
//        System.out.println("---------------------------------");
    }

    @Test
//    @Ignore
    public void loginFailTest() throws FileNotFoundException, IOException {
        PortalSession portal = new PortalSession();

        HttpEntity res = portal.loginPortal("12345678", "11234511");

        FileOutputStream fos;
        fos = new FileOutputStream("loginFailTest.log");

        String text = "";
        text += "loginFailTest\n";
        text += res.getHeaders() + "\n\n";
        text += res.getBody() + "\n\n";

        fos.write(text.getBytes());
        fos.close();

//        System.out.println("loginFailTest");
//        System.out.println(res.getHeaders());
//        System.out.println("---------------------------------");
    }
}
