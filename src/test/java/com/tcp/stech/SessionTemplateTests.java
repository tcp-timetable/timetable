package com.tcp.stech;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

/**
 * Created by janpr on 2016-09-26.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.tcp.timetable.TimetableApplication.class)
public class SessionTemplateTests {

    @Test
    public void getTest() {
        SessionTemplate session = new SessionTemplate();

        HttpHeaders headers = new HttpHeaders();

        HttpEntity res = session.get("http://portal.seoultech.ac.kr", headers, new HashMap());

//        System.out.println("getTest");
//        System.out.println(res.getHeaders());
//        System.out.println("---------------------------------");
    }
}
